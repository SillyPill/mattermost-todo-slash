#!/usr/bin/env python

from http.server import BaseHTTPRequestHandler, HTTPServer
import urllib.parse as urlparse
import json
import sqlite3
import datetime
from dateutil.relativedelta import relativedelta

# Define server address and port, use localhost if you are running this on your Mattermost server.
HOSTNAME = '0.0.0.0'
PORT = 8000
TOKEN = ""

# guarantee unicode string
_u = lambda t: t.decode('UTF-8', 'replace') if isinstance(t, str) else t


class MattermostRequest(object):
    def __init__(self, response_url=None, text=None, token=None, channel_id=None, team_id=None, command=None,
                 team_domain=None, user_name=None, channel_name=None):
        self.response_url = response_url
        self.text = text
        self.token = token
        self.channel_id = channel_id
        self.team_id = team_id
        self.command = command
        self.team_domain = team_domain
        self.user_name = user_name
        self.channel_name = channel_name


class PostHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        """Respond to a POST request."""
        # Extract the contents of the POST
        length = int(self.headers['Content-Length'])
        post_data = urlparse.parse_qs(self.rfile.read(length).decode('utf-8'))

        # Get POST data and initialize MattermostRequest object
        for key, value in post_data.items():
            if key == 'response_url':
                MattermostRequest.response_url = value
            elif key == 'text':
                MattermostRequest.text = value[0]
            elif key == 'token':
                MattermostRequest.token = value
            elif key == 'channel_id':
                MattermostRequest.channel_id = value[0]
            elif key == 'team_id':
                MattermostRequest.team_id = value
            elif key == 'command':
                MattermostRequest.command = value
            elif key == 'team_domain':
                MattermostRequest.team_domain = value
            elif key == 'user_name':
                MattermostRequest.user_name = value
            elif key == 'channel_name':
                MattermostRequest.channel_name = value

        responsetext = ''

        # Here we trigger the command
        if MattermostRequest.token == TOKEN:
           if MattermostRequest.command[0] == u'/todo' or MattermostRequest.command[0] == u'/TODO':
                responsetext, ephemeral_response = ProcessToDo(MattermostRequest.text, MattermostRequest.channel_id, MattermostRequest.user_name[0])

        if responsetext:
            data = {}
            # 'response_type' may also be 'in_channel'

            if ephemeral_response:
                data['response_type'] = 'ephemeral'
            else:
                data['response_type'] = 'in_channel'

            data['text'] = responsetext
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(bytes( json.dumps(data).encode() ))
        return

def delete_TODO( Channel_Id, user_name, n ):

    cur = conn.execute("SELECT id,item FROM todo WHERE ChannelID = ? ORDER BY deadline LIMIT 1 OFFSET ?;",[Channel_Id, n-1])
    todo_id = cur.fetchall()


    if len(todo_id) != 1:
        response_text = "Invalid Todo_Index bro.."
        return response_text, True

    item = todo_id[0][1]
    conn.execute("DELETE FROM todo WHERE id = ?", [todo_id[0][0]])

    response_text = "@{0} erased \"{1}\" from the TODO list".format(user_name, item)
    conn.commit()
    return response_text, False

def list_TODO( Channel_Id ):
    cur = conn.execute("SELECT item, deadline FROM todo WHERE ChannelID = ? ORDER BY deadline",[Channel_Id,])
    resultList = []
    for row in cur:
        resultList.append( ( row[0], row[1] ) )
    return resultList

def insert_TODO(channelID, item, deadline=None):
    if deadline:
        deadline = datetime.datetime(deadline.year, deadline.month, deadline.day)
        timestamp = (deadline - datetime.datetime(1970, 1, 1)) / datetime.timedelta(seconds=1)
        print( channelID, " ", item, " ", timestamp )
        conn.execute("INSERT INTO todo ( ChannelID, item, deadline ) VALUES ( ?, ?, ? )", (channelID, item, timestamp))
        conn.commit()
    else:
        print( channelID, " ", item, " ")
        conn.execute("INSERT INTO todo ( ChannelID, item ) VALUES ( ?, ? )", (channelID, item))
        conn.commit()

def ProcessToDo(text, channel_id, user_name):
    # text = text.translate({ord(c): " " for c in "!@#$%^&*()[]{};:,./<>?\|`~-=_+"})
    words = text.split()

    if len(words) == 1 :

        if (words[0].lower() in ['hello', 'hi', 'hey']):
            response_text = " ![alt text](http://i.imgur.com/jxGyuDx.jpg\"Hey!\") "
            return response_text, True

        elif (words[0].lower() in ['list', 'show']):

            response_text = "### This is our TODO list\n\n"

            todo_list = list_TODO(channel_id)

            response_text += u'| No. | Item | Deadline |\n'
            response_text += u'| ----: | :---- | :---- |\n'

            for idx,item in enumerate(todo_list, start=1):
                if item[1]:
                    deadline = item[1]
                    present_timestamp = datetime.datetime.now().timestamp()
                    time_left = deadline - present_timestamp
                    if time_left < 0:
                        deadline = u"*due*"
                    else:
                        days_left = int(time_left/(60*60*24))
                        if days_left == 0:
                            deadline = u"today"
                        elif days_left == 1:
                            deadline = u"tomorrow"
                        else:
                            deadline = str(days_left) +  " days"
                else:
                    deadline = " "
                response_text += u"| {0} | {1} | {2} |\n".format(str(idx), str(item[0]), str(deadline))
            return response_text, True

    elif len(words) == 2 and words[0] in ['delete', 'remove', 'check', 'done'] and words[1].isnumeric():
        n = int(words[1])
        response_text, ephemeral_response = delete_TODO(channel_id, user_name, n)
        return response_text, ephemeral_response



    elif len(words) <= 3:
            item =' '.join(word for word in words) 
            insert_TODO(channel_id, item)
            response_text = "@{0} added item \"{1}\" on the Todo list".format(user_name, item)
            return response_text, False

    else: 

        deadline = -1

        if (words[-3].lower(), words[-2].lower(), words[-1].lower()) in [ ("by", "next", "week!"), ("by", "next", "week") ]:
            deadline = datetime.date.today() + datetime.timedelta(days=7) 
            deadline = deadline - datetime.timedelta(days=deadline.weekday())
            item =' '.join(word for word in words[:-3]) 

        elif (words[-3].lower(), words[-2].lower(), words[-1].lower()) in [ ("by", "next", "month!"), ("by", "next", "month") ]:
            deadline = datetime.date.today() + datetime.timedelta(days=30) 
            deadline = date(deadline.year, deadline.month, 1)
            item =' '.join(word for word in words[:-3]) 

        elif (words[-2].lower(), words[-1].lower()) in [ ("next", "week!"), ("next", "week") ]:
            deadline = datetime.date.today() + datetime.timedelta(days=7) 
            item =' '.join(word for word in words[:-2]) 

        elif words[-1].lower() in ["tomorrow", "tomorrow!"]:
            deadline = datetime.date.today() + datetime.timedelta(days=1) 
            item =' '.join(word for word in words[:-1]) 

        elif (words[-2].lower(), words[-1].lower()) in [ ("next", "month!"), ("next", "month") ]:
            deadline = datetime.date.today() + datetime.timedelta(days=30) 
            item =' '.join(word for word in words[:-2]) 

        elif (words[-3].lower(),words[-2].lower(), words[-1].lower()) in [ ("day", "after", "tomorrow!"), ("day",  "after", "tomorrow") ]:
            deadline = datetime.date.today() + datetime.timedelta(days=2) 
            item =' '.join(word for word in words[:-3]) 

        elif (words[-3].lower(), words[-1].lower()) in [ ("in", "days!"), ("in", "days") ] and words[-2].isnumeric():
            deadline = datetime.date.today() + datetime.timedelta(days=int(words[-2])) 
            item =' '.join(word for word in words[:-3]) 

        elif (words[-3].lower(), words[-1].lower()) in [ ("in", "weeks!"), ("in", "weeks") ] and words[-2].isnumeric():
            deadline = datetime.date.today() + datetime.timedelta(days=(int(words[-2])*7)) 
            item =' '.join(word for word in words[:-3]) 

        elif (words[-3].lower(), words[-1].lower()) in [ ("in", "months!"), ("in", "months") ] and words[-2].isnumeric():
            deadline = datetime.date.today() + datetime.timedelta(days=(int(words[-2])*30)) 
            item =' '.join(word for word in words[:-3]) 

        elif ( words[-1].lower()) in [ ( "days!"), ( "days" ) ] and words[-2].isnumeric():
            deadline = datetime.date.today() + datetime.timedelta(days=int(words[-2])) 
            item =' '.join(word for word in words[:-3]) 

        elif ( words[-1].lower()) in [ ( "weeks!"), ( "weeks" ) ] and words[-2].isnumeric():
            deadline = datetime.date.today() + datetime.timedelta(days=(int(words[-2])*7)) 
            item =' '.join(word for word in words[:-3]) 

        elif ( words[-1].lower()) in [ ( "months!"), ( "months" ) ] and words[-2].isnumeric():
            deadline = datetime.date.today() + datetime.timedelta(days=(int(words[-2])*30)) 
            item =' '.join(word for word in words[:-3]) 

        else:
            item =' '.join(word for word in words) 

        if deadline == -1:
            insert_TODO(channel_id, item)
            response_text = "@{0} added item \"{1}\" on the TODO list".format(user_name, item)
        else :
            insert_TODO(channel_id, item, deadline)
            response_text = "@{0} added item \"{1}\" on the TODO list".format(user_name, item)

        return response_text, False




if __name__ == '__main__':
    # from BaseHTTPServer import HTTPServer
    server = HTTPServer((HOSTNAME, PORT), PostHandler)
    print('Starting matterslash server, use <Ctrl-C> to stop')
    conn = sqlite3.connect('todo.db') 
    conn.execute('CREATE TABLE IF NOT EXISTS todo (id INTEGER PRIMARY KEY AUTOINCREMENT, ChannelID text, item text, deadline INTEGER)')
    server.serve_forever()


